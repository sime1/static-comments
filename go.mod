module comments

go 1.13

require (
	github.com/aws/aws-lambda-go v1.13.3
	github.com/google/go-github v17.0.0+incompatible
	github.com/google/go-github/v28 v28.1.1
	github.com/json-iterator/go v1.1.9
	github.com/sime1/comments v0.0.0-00010101000000-000000000000
	github.com/spf13/viper v1.6.1
	golang.org/x/oauth2 v0.0.0-20180821212333-d2e6202438be
	gopkg.in/yaml.v2 v2.2.4
)

replace github.com/sime1/comments => ./.
