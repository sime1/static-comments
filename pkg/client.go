package comments

import (
	"context"
	"github.com/google/go-github/v28/github"
	"github.com/spf13/viper"
	"golang.org/x/oauth2"
)

func init() {
	viper.SetEnvPrefix("COMMENT")
	viper.AutomaticEnv()
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.SetConfigType("yaml")
	viper.ReadInConfig()
}

// Client is used to interact with github at an higher abstraction
type Client struct {
	*Configuration
	github *github.Client
}

type Configuration struct {
	user        string
	repo        string
	commentPath string
}

// New creates a new Client
func NewClient() *Client {
	token := viper.GetString("token")
	user := viper.GetString("user")
	repo := viper.GetString("repo")
	path := viper.GetString("comment_path")

	ctx := context.Background()
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: token},
	)
	tc := oauth2.NewClient(ctx, ts)
	gh := github.NewClient(tc)
	return &Client{
		Configuration: &Configuration{
			user:        user,
			repo:        repo,
			commentPath: path,
		},
		github: gh,
	}
}
