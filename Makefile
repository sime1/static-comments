.PHONY: clean conf build

default: clean build conf

clean:
	rm -rf ./build
	
build:
	go build -o build/comments cmd/main.go

conf:
	cp config.yaml build/config.yaml