package main

import (
	"context"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	json "github.com/json-iterator/go"

	comments "github.com/sime1/comments/pkg"
)

var client *comments.Client
var cors map[string]string = map[string]string{
	"Access-Control-Allow-Origin": "*",
}

func init() {
	client = comments.NewClient()
}

func main() {
	lambda.Start(CommentHandler)
}

func CommentHandler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	comment := &comments.Comment{}
	if err := json.UnmarshalFromString(req.Body, comment); err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 503, Body: err.Error(), Headers: cors}, nil
	}
	if err := client.CreateComment(ctx, comment); err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 503, Body: err.Error(), Headers: cors}, nil
	}
	return events.APIGatewayProxyResponse{StatusCode: 200, Body: "OK", Headers: cors}, nil
}
